package main

import (
	"context"
	"flag"
	"github.com/camunda/zeebe/clients/go/v8/pkg/zbc"
	"github.com/rs/zerolog/log"
	_ "gitlab.com/shadowy/go/zerolog-settings"
)

func main() {
	var flowID = flag.String("flow-id", "", "flow id")
	var server = flag.String("server", "", "server")
	flag.Parse()
	l := log.Logger.With().Str("flowID", *flowID).Str("server", *server).Logger()
	l.Info().Msg("new-process")
	client, err := zbc.NewClient(&zbc.ClientConfig{
		GatewayAddress:         *server,
		UsePlaintextConnection: true,
	})

	if err != nil {
		l.Error().Err(err).Msg("create client")
		return
	}

	//req, err := client.NewPublishMessageCommand().MessageName("approved").CorrelationKey("100").VariablesFromMap(map[string]interface{}{"id1": "100"})
	req, err := client.NewCreateInstanceCommand().BPMNProcessId(*flowID).LatestVersion().VariablesFromMap(map[string]interface{}{"id": "100"})
	if err != nil {
		l.Error().Err(err).Msg("new process start")
		return
	}
	response, err := req.Send(context.Background())
	if err != nil {
		l.Error().Err(err).Msg("new process start")
		return
	}

	l.Debug().Str("response", response.String()).Msg("new process start")
}
