package main

import (
	"context"
	"flag"
	"github.com/camunda/zeebe/clients/go/v8/pkg/entities"
	"github.com/camunda/zeebe/clients/go/v8/pkg/worker"
	"github.com/camunda/zeebe/clients/go/v8/pkg/zbc"
	"github.com/rs/zerolog/log"
	_ "gitlab.com/shadowy/go/zerolog-settings"
)

func main() {
	var server = flag.String("server", "", "server")
	flag.Parse()
	l := log.Logger.With().Str("server", *server).Logger()
	l.Info().Msg("task-send")
	client, err := zbc.NewClient(&zbc.ClientConfig{
		GatewayAddress:         *server,
		UsePlaintextConnection: true,
	})

	if err != nil {
		l.Error().Err(err).Msg("create client")
		return
	}

	var readyClose = make(chan struct{})
	jobWorker := client.NewJobWorker().JobType("send").Handler(taskSend).Open()
	<-readyClose
	jobWorker.Close()
	jobWorker.AwaitClose()
}

func taskSend(client worker.JobClient, job entities.Job) {
	jobKey := job.Key
	l := log.Logger.With().Int64("jobKey", jobKey).Str("flowID", job.BpmnProcessId).Logger()
	l.Debug().Msg("taskSend start")
	response, err := client.NewCompleteJobCommand().JobKey(jobKey).Send(context.Background())
	if err != nil {
		l.Error().Err(err).Msg("taskSend send")
		return
	}
	l.Debug().Str("response", response.String()).Msg("taskSend end")
}
