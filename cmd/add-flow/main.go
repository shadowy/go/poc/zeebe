package main

import (
	"context"
	"flag"
	"github.com/camunda/zeebe/clients/go/v8/pkg/zbc"
	"github.com/rs/zerolog/log"
	_ "gitlab.com/shadowy/go/zerolog-settings"
)

func main() {
	var file = flag.String("file", "", "file with flow")
	var server = flag.String("server", "", "server")
	flag.Parse()
	l := log.Logger.With().Str("file", *file).Str("server", *server).Logger()
	l.Info().Msg("add-flow")
	client, err := zbc.NewClient(&zbc.ClientConfig{
		GatewayAddress:         *server,
		UsePlaintextConnection: true,
	})

	if err != nil {
		l.Error().Err(err).Msg("create client")
		return
	}

	response, err := client.NewDeployResourceCommand().AddResourceFile(*file).Send(context.Background())
	if err != nil {
		l.Error().Err(err).Msg("upload flow")
		return
	}

	l.Debug().Str("response", response.String()).Msg("upload flow")
}
